package com.bourntec.bankingsystem.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class RecordNotFoundException extends RuntimeException {
String errorMessage;
}
