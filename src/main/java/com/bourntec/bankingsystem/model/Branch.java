package com.bourntec.bankingsystem.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@EntityListeners(AuditingEntityListener.class)
//@Table(name="branch")
public class Branch {
	@Column(length=1)
	private String recordStatus;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	private Integer branchId;
	private String bankName;
	private String branchName;
	private String ifscCode;
	@CreatedDate
	private LocalDateTime updatedDate;
	@LastModifiedDate
	private LocalDateTime lastModifiedDate;
}
