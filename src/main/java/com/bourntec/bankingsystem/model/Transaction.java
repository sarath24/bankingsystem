package com.bourntec.bankingsystem.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@EntityListeners(AuditingEntityListener.class)
//@Table(name="transaction")
//@Data
public class Transaction {
	@Column(length = 1)
	private String recordStatus;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer transactionId;
	private Integer referenceId;
	private Integer amount;
	@Enumerated(EnumType.STRING)
	private TransactionType type;
	private String notification;
	@CreatedDate
	private LocalDateTime updatedDate;
	@LastModifiedDate
	private LocalDateTime lastModifiedDate;
}
