package com.bourntec.bankingsystem.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bourntec.bankingsystem.model.Branch;
import com.bourntec.bankingsystem.request.BranchRequestDTO;
import com.bourntec.bankingsystem.response.BranchResponseDTO;
import com.bourntec.bankingsystem.service.BranchService;
@RestController
@RequestMapping("/branches")
public class BranchController {
	
	@Autowired
	BranchService branchService;
    @GetMapping
	public List<BranchResponseDTO> getAll() {
	return branchService.getAll();
	}
    @DeleteMapping("/active/{id}")
    public void deleteByBranchId(@PathVariable int id) {
    branchService.deleteByBranchId(id);
    }

    @GetMapping("/{id}")
    public BranchResponseDTO get(@PathVariable int branchId) {
    return branchService.findByBranchId(branchId);
    }
    @PostMapping("/bulk")
    public List<BranchResponseDTO> createAll(@RequestBody List<BranchRequestDTO> branchRequestDTO) {
    return branchService.saveAll(branchRequestDTO);
    } 
    @PutMapping("/{id}")
    public BranchResponseDTO update(@PathVariable int branchId, @RequestBody BranchRequestDTO branchRequestDTO) {
    return branchService.update(branchId, branchRequestDTO);
    } 
    @PostMapping
    public BranchResponseDTO create(@RequestBody @Valid BranchRequestDTO branchRequestDTO) {
    return branchService.save(branchRequestDTO);
    }


    }


