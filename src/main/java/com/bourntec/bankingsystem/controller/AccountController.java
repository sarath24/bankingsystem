package com.bourntec.bankingsystem.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bourntec.bankingsystem.model.Account;
import com.bourntec.bankingsystem.request.AccountRequestDTO;
import com.bourntec.bankingsystem.response.AccountResponseDTO;
import com.bourntec.bankingsystem.service.AccountService;

@RestController
@RequestMapping("/accounts")

public class AccountController {
	
	@Autowired
	AccountService accountService;
    @GetMapping
	public List<AccountResponseDTO> getAll() {
	return accountService.getAll();
	}
    @DeleteMapping("/active/{id}")
    public void deleteByAccountId(@PathVariable int id) {
    accountService.deleteByAccountId(id);
    }
    @GetMapping("/{id}")
    public AccountResponseDTO get( @PathVariable int accountId) {
    return accountService.findByAccountId( accountId);
    }
    @PostMapping("/bulk")
    public List<AccountResponseDTO> createAll(@RequestBody List<AccountRequestDTO> accountRequestDTOList) {
    return accountService.saveAll(accountRequestDTOList); 
    }
    @PutMapping("/{id}")
    public AccountResponseDTO update(@PathVariable int accountId, @RequestBody AccountRequestDTO accountRequestDTO) {
    return accountService.update(accountId, accountRequestDTO);
    }
    @PostMapping
    public AccountResponseDTO create(@RequestBody @Valid AccountRequestDTO accountRequestDTO) {
    return accountService.save(accountRequestDTO);
    }


	}

