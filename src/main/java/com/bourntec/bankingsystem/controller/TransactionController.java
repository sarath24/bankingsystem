package com.bourntec.bankingsystem.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bourntec.bankingsystem.model.Customer;
import com.bourntec.bankingsystem.model.Transaction;
import com.bourntec.bankingsystem.request.CustomerRequestDTO;
import com.bourntec.bankingsystem.request.TransactionRequestDTO;
import com.bourntec.bankingsystem.response.CustomerResponseDTO;
import com.bourntec.bankingsystem.response.TransactionResponseDTO;
import com.bourntec.bankingsystem.service.TransactionService;
@RestController
@RequestMapping("/transactions")
public class TransactionController {
	@Autowired
	TransactionService transactionService;
    @GetMapping
	public List<TransactionResponseDTO> getAll() {
	return transactionService.getAll();
	}
    @DeleteMapping("/active/{id}")
    public void deleteByTransactionId(@PathVariable int transactionId) {
    transactionService.deleteByTransactionId(transactionId);
    }
    @GetMapping("/{id}")
    public TransactionResponseDTO get( @PathVariable int transactionId) {
    return transactionService.findByTransactionId(transactionId);
    }
    @PostMapping("/bulk")
    public List<TransactionResponseDTO> createAll(@RequestBody List<TransactionRequestDTO> transactionRequestDTO) {
    return transactionService.saveAll(transactionRequestDTO);
    } 
    @PutMapping("/{id}")
    public  TransactionResponseDTO update(@PathVariable int transactionId, @RequestBody TransactionRequestDTO transactionRequestDTO) {
    return transactionService.update(transactionId, transactionRequestDTO);
    } 
    @PostMapping
    public TransactionResponseDTO create(@RequestBody @Valid TransactionRequestDTO transactionRequestDTO) {
    return transactionService.save(transactionRequestDTO);
    }
	}


