package com.bourntec.bankingsystem.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bourntec.bankingsystem.model.Branch;
import com.bourntec.bankingsystem.model.Customer;
import com.bourntec.bankingsystem.request.BranchRequestDTO;
import com.bourntec.bankingsystem.request.CustomerRequestDTO;
import com.bourntec.bankingsystem.response.BranchResponseDTO;
import com.bourntec.bankingsystem.response.CustomerResponseDTO;
import com.bourntec.bankingsystem.service.CustomerService;

@RestController
@RequestMapping("/customers")
public class CustomerController {
	@Autowired
	CustomerService customerService;
    @GetMapping
	public List<CustomerResponseDTO> getAll() {
	return customerService.getAll();
	}
    @DeleteMapping("/active/{id}")
    public void deleteByCustomerId(@PathVariable int customerId) {
    customerService.deleteByCustomerId(customerId);
    }
    @GetMapping("/{id}")
    public CustomerResponseDTO get(@PathVariable int customerId) {
    return customerService.findByCustomerId(customerId);
    }
    @PostMapping("/bulk")
    public List<CustomerResponseDTO> createAll(@RequestBody List<CustomerRequestDTO> customerRequestDTO) {
    return customerService.saveAll(customerRequestDTO);
    } 
    @PutMapping("/{id}")
    public CustomerResponseDTO update(@PathVariable int customerId, @RequestBody CustomerRequestDTO  customerRequestDTO) {
    return customerService.update(customerId,  customerRequestDTO);
    } 
    @PostMapping
    public CustomerResponseDTO create(@RequestBody @Valid CustomerRequestDTO customerRequestDTO) {
    return customerService.save(customerRequestDTO);
    }
	}

