package com.bourntec.bankingsystem.service;

import java.util.List;

import com.bourntec.bankingsystem.model.Customer;
import com.bourntec.bankingsystem.model.Transaction;
import com.bourntec.bankingsystem.request.CustomerRequestDTO;
import com.bourntec.bankingsystem.request.TransactionRequestDTO;
import com.bourntec.bankingsystem.response.CustomerResponseDTO;
import com.bourntec.bankingsystem.response.TransactionResponseDTO;

public interface TransactionService {
	List<TransactionResponseDTO> getAll();

    void deleteByTransactionId(int transactionId);
    
    TransactionResponseDTO findByTransactionId(int transactionId);

    TransactionResponseDTO update(int transactionId, TransactionRequestDTO transactionRequestDTO);
    
    TransactionResponseDTO save(TransactionRequestDTO transactionRequestDTO);

   List<TransactionResponseDTO> saveAll(List<TransactionRequestDTO> transactionRequestDTO);
}
