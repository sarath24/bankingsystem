package com.bourntec.bankingsystem.service;

import java.util.List;

import com.bourntec.bankingsystem.model.Account;
import com.bourntec.bankingsystem.model.Branch;
import com.bourntec.bankingsystem.model.Customer;
import com.bourntec.bankingsystem.request.AccountRequestDTO;
import com.bourntec.bankingsystem.request.CustomerRequestDTO;
import com.bourntec.bankingsystem.response.AccountResponseDTO;
import com.bourntec.bankingsystem.response.CustomerResponseDTO;


public interface CustomerService {
	List<CustomerResponseDTO> getAll();

      void deleteByCustomerId(int customerId);
      
      CustomerResponseDTO findByCustomerId (int customerId);

      CustomerResponseDTO update(int customerId, CustomerRequestDTO customerRequestDTO );
      
      CustomerResponseDTO save(CustomerRequestDTO customerRequestDTO);

     List<CustomerResponseDTO> saveAll(List<CustomerRequestDTO> customerRequestDTO);
}
