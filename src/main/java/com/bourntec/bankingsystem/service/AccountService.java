package com.bourntec.bankingsystem.service;

import java.util.List;

import com.bourntec.bankingsystem.model.Account;
import com.bourntec.bankingsystem.request.AccountRequestDTO;
import com.bourntec.bankingsystem.response.AccountResponseDTO;


public interface AccountService {
	List<AccountResponseDTO> getAll();

     void deleteByAccountId(int accountId);
     
     AccountResponseDTO findByAccountId(int accountId);

     AccountResponseDTO update(int id, AccountRequestDTO accountRequestDTO);

     AccountResponseDTO save(AccountRequestDTO accountRequestDTO);

    List<AccountResponseDTO> saveAll(List<AccountRequestDTO> accountRequestDTOList);
}
