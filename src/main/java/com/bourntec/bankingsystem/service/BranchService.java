package com.bourntec.bankingsystem.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bourntec.bankingsystem.model.Account;
import com.bourntec.bankingsystem.model.Branch;
import com.bourntec.bankingsystem.request.AccountRequestDTO;
import com.bourntec.bankingsystem.request.BranchRequestDTO;
import com.bourntec.bankingsystem.response.AccountResponseDTO;
import com.bourntec.bankingsystem.response.BranchResponseDTO;

@Service
public interface BranchService {
	List<BranchResponseDTO> getAll();

    void deleteByBranchId(int branchId);
    
    BranchResponseDTO findByBranchId( int branchId);

    BranchResponseDTO update(int branchid, BranchRequestDTO branchRequestDTO);

    BranchResponseDTO save(BranchRequestDTO BranchRequestDTO);

   List<BranchResponseDTO> saveAll(List<BranchRequestDTO> branchRequestDTO);
}
