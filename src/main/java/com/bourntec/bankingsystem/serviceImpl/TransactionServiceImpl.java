package com.bourntec.bankingsystem.serviceImpl;

import java.util.ArrayList;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.bourntec.bankingsystem.exception.RecordNotFoundException;
import com.bourntec.bankingsystem.model.Branch;
import com.bourntec.bankingsystem.model.Transaction;
import com.bourntec.bankingsystem.repository.TransactionRepository;
import com.bourntec.bankingsystem.request.BranchRequestDTO;
import com.bourntec.bankingsystem.request.TransactionRequestDTO;
import com.bourntec.bankingsystem.response.BranchResponseDTO;
import com.bourntec.bankingsystem.response.TransactionResponseDTO;
import com.bourntec.bankingsystem.service.TransactionService;
import com.bourntec.bankingsystem.util.Constants;

public class TransactionServiceImpl implements TransactionService {
	@Autowired
	TransactionRepository transactionRepository;
	@Override
	public List<TransactionResponseDTO> getAll() {
	return transactionRepository.findByRecordStatus(Constants.ACTIVE).stream().map(TransactionResponseDTO::new)
	.toList(); } 
	@Override
	public void deleteByTransactionId(int transactionId) { Transaction transaction = transactionRepository.findByRecordStatusAndtransactionId(Constants.ACTIVE, transactionId);
	if (transaction != null) {
	transaction.setRecordStatus(Constants.DELETE);
	transactionRepository.save(transaction);
	} else {
	System.out.println("not found!");
	}
	}
	@Override
	public TransactionResponseDTO findByTransactionId(int transactionId) {

		Transaction transactionOptional = transactionRepository.findByRecordStatusAndtransactionId(Constants.ACTIVE, transactionId);
		if (transactionOptional != null)
			return new TransactionResponseDTO(transactionOptional);
		else
			throw new RecordNotFoundException("Data not exist");

	}

	@Override
	public TransactionResponseDTO update(int id, TransactionRequestDTO transactionRequestDTO) {
		Transaction existingTransaction = transactionRepository.findByRecordStatusAndtransactionId(Constants.ACTIVE, id);
		if (existingTransaction != null) {
			Transaction transaction = transactionRequestDTO.convertToModel();
			transaction.setRecordStatus(Constants.ACTIVE);
			transaction.setUpdatedDate(existingTransaction.getUpdatedDate());
			transaction = transactionRepository.save(transaction);
			return new TransactionResponseDTO(transaction);
		} else
			throw new RecordNotFoundException("Record does not exist");
	}

	@Override
	public TransactionResponseDTO save(TransactionRequestDTO transactionRequestDTO) {
		Transaction transaction = transactionRequestDTO.convertToModel();
		transaction.setRecordStatus(Constants.ACTIVE);
		transaction = transactionRepository.save(transaction);
		return new TransactionResponseDTO(transaction);
	}

	@Override
	public List<TransactionResponseDTO> saveAll(List<TransactionRequestDTO> transactionRequestDTOList) {
		List<Transaction> transactionList = new ArrayList<>();
		Transaction transaction = null;
		for (TransactionRequestDTO transactionRequestDTO : transactionRequestDTOList) {
			transaction = transactionRequestDTO.convertToModel();
			transaction.setRecordStatus(Constants.ACTIVE);
			transactionList.add(transaction);
		}
		return transactionRepository.saveAll(transactionList).stream().map(TransactionResponseDTO::new).toList();
	
	}
	
		
	}


