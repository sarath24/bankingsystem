package com.bourntec.bankingsystem.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.bourntec.bankingsystem.exception.RecordNotFoundException;
import com.bourntec.bankingsystem.model.Account;
import com.bourntec.bankingsystem.model.Branch;
import com.bourntec.bankingsystem.repository.AccountRepository;
import com.bourntec.bankingsystem.repository.BranchRepository;
import com.bourntec.bankingsystem.request.AccountRequestDTO;
import com.bourntec.bankingsystem.request.BranchRequestDTO;
import com.bourntec.bankingsystem.response.AccountResponseDTO;
import com.bourntec.bankingsystem.response.BranchResponseDTO;
import com.bourntec.bankingsystem.service.BranchService;
import com.bourntec.bankingsystem.util.Constants;

public class BranchServiceImpl implements BranchService {
	@Autowired
	BranchRepository branchRepository;
	@Override
	public List<BranchResponseDTO> getAll() {
		return branchRepository.findByRecordStatus(Constants.ACTIVE).stream().map(BranchResponseDTO::new).toList();

	}

	@Override
	public void deleteByBranchId(int branchId) {
		Branch branch = branchRepository.findByRecordStatusAndbranchId(Constants.ACTIVE, branchId);
		if (branch != null) {
			branch.setRecordStatus(Constants.DELETE);
			branchRepository.save(branch);
		} else {
			System.out.println("not found!");
		}
	}

	@Override
	public BranchResponseDTO findByBranchId(int branchId) {

		Branch branchOptional = branchRepository.findByRecordStatusAndbranchId(Constants.ACTIVE, branchId);
		if (branchOptional != null)
			return new BranchResponseDTO(branchOptional);
		else
			throw new RecordNotFoundException("Data not exist");

	}

	@Override
	public BranchResponseDTO update(int id, BranchRequestDTO branchRequestDTO) {
		Branch existingBranch = branchRepository.findByRecordStatusAndbranchId(Constants.ACTIVE, id);
		if (existingBranch != null) {
			Branch branch = branchRequestDTO.convertToModel();
			branch.setRecordStatus(Constants.ACTIVE);
			branch.setUpdatedDate(existingBranch.getUpdatedDate());
			branch = branchRepository.save(branch);
			return new BranchResponseDTO(branch);
		} else
			throw new RecordNotFoundException("Record does not exist");
	}

	@Override
	public BranchResponseDTO save(BranchRequestDTO branchRequestDTO) {
		Branch branch = branchRequestDTO.convertToModel();
		branch.setRecordStatus(Constants.ACTIVE);
		branch = branchRepository.save(branch);
		return new BranchResponseDTO(branch);
	}

	@Override
	public List<BranchResponseDTO> saveAll(List<BranchRequestDTO> branchRequestDTOList) {
		List<Branch> branchList = new ArrayList<>();
		Branch branch = null;
		for (BranchRequestDTO branchRequestDTO : branchRequestDTOList) {
			branch = branchRequestDTO.convertToModel();
			branch.setRecordStatus(Constants.ACTIVE);
			branchList.add(branch);
		}
		return branchRepository.saveAll(branchList).stream().map(BranchResponseDTO::new).toList();
	}
		
	}




