package com.bourntec.bankingsystem.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.bourntec.bankingsystem.exception.RecordNotFoundException;
import com.bourntec.bankingsystem.model.Branch;
import com.bourntec.bankingsystem.model.Customer;
import com.bourntec.bankingsystem.repository.CustomerRepository;
import com.bourntec.bankingsystem.request.BranchRequestDTO;
import com.bourntec.bankingsystem.request.CustomerRequestDTO;
import com.bourntec.bankingsystem.response.BranchResponseDTO;
import com.bourntec.bankingsystem.response.CustomerResponseDTO;
import com.bourntec.bankingsystem.service.CustomerService;
import com.bourntec.bankingsystem.util.Constants;

public class CustomerServiceImpl implements CustomerService {
	@Autowired
	CustomerRepository customerRepository;
	@Override
	public List<CustomerResponseDTO> getAll() {
	return customerRepository.findByRecordStatus(Constants.ACTIVE).stream().map(CustomerResponseDTO::new)
	.toList(); } 
	@Override
	public void deleteByCustomerId(int customerId) { Customer customer = customerRepository.findByRecordStatusAndCustomerId(Constants.ACTIVE, customerId);
	if (customer != null) {
	customer.setRecordStatus(Constants.DELETE);
	customerRepository.save(customer);
	} else {
	System.out.println("not found!");
	}
	}
	@Override
	public CustomerResponseDTO findByCustomerId(int customerId) {

		 Customer  customerOptional =  customerRepository.findByRecordStatusAndCustomerId(Constants.ACTIVE, customerId);
		if ( customerOptional != null)
			return new  CustomerResponseDTO( customerOptional);
		else
			throw new RecordNotFoundException("Data not exist");

	}

	@Override
	public CustomerResponseDTO update(int id, CustomerRequestDTO  customerRequestDTO) {
		 Customer existingCustomer =  customerRepository.findByRecordStatusAndCustomerId(Constants.ACTIVE, id);
		if (existingCustomer != null) {
			Customer  customer =  customerRequestDTO.convertToModel();
			 customer.setRecordStatus(Constants.ACTIVE);
			 customer.setUpdatedDate(existingCustomer.getUpdatedDate());
			 customer =  customerRepository.save( customer);
			return new CustomerResponseDTO( customer);
		} else
			throw new RecordNotFoundException("Record does not exist");
	}

	@Override
	public CustomerResponseDTO save(CustomerRequestDTO  customerRequestDTO) {
		Customer  customer =  customerRequestDTO.convertToModel();
		 customer.setRecordStatus(Constants.ACTIVE);
		 customer =  customerRepository.save( customer);
		return new CustomerResponseDTO( customer);
	}

	@Override
	public List<CustomerResponseDTO> saveAll(List<CustomerRequestDTO>  customerRequestDTOList) {
		List<Customer>  customerList = new ArrayList<>();
		Customer  customer = null;
		for (CustomerRequestDTO  customerRequestDTO :  customerRequestDTOList) {
			 customer =  customerRequestDTO.convertToModel();
			 customer.setRecordStatus(Constants.ACTIVE);
			 customerList.add( customer);
		}
		return customerRepository.saveAll( customerList).stream().map( CustomerResponseDTO::new).toList();
	}
	
	}




