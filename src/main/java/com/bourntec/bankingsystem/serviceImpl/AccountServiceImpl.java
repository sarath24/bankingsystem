package com.bourntec.bankingsystem.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bourntec.bankingsystem.exception.RecordNotFoundException;
import com.bourntec.bankingsystem.model.Account;
import com.bourntec.bankingsystem.repository.AccountRepository;
import com.bourntec.bankingsystem.request.AccountRequestDTO;
import com.bourntec.bankingsystem.response.AccountResponseDTO;
import com.bourntec.bankingsystem.service.AccountService;
import com.bourntec.bankingsystem.util.Constants;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	AccountRepository accountRepository;

	@Override
	public List<AccountResponseDTO> getAll() {
		return accountRepository.findByRecordStatus(Constants.ACTIVE).stream().map(AccountResponseDTO::new).toList();

	}

	@Override
	public void deleteByAccountId(int accountId) {
		Account account = accountRepository.findByRecordStatusAndAccountId(Constants.ACTIVE, accountId);
		if (account != null) {
			account.setRecordStatus(Constants.DELETE);
			accountRepository.save(account);
		} else {
			System.out.println("not found!");
		}
	}

	@Override
	public AccountResponseDTO findByAccountId(int accountId) {

		Account accountOptional = accountRepository.findByRecordStatusAndAccountId(Constants.ACTIVE, accountId);
		if (accountOptional != null)
			return new AccountResponseDTO(accountOptional);
		else
			throw new RecordNotFoundException("Data not exist");

	}

	@Override
	public AccountResponseDTO update(int id, AccountRequestDTO accountRequestDTO) {
		Account existingAccount = accountRepository.findByRecordStatusAndAccountId(Constants.ACTIVE, id);
		if (existingAccount != null) {
			Account account = accountRequestDTO.convertToModel();
			account.setRecordStatus(Constants.ACTIVE);
			account.setUpdatedDate(existingAccount.getUpdatedDate());
			account = accountRepository.save(account);
			return new AccountResponseDTO(account);
		} else
			throw new RecordNotFoundException("Record does not exist");
	}

	@Override
	public AccountResponseDTO save(AccountRequestDTO accountRequestDTO) {
		Account account = accountRequestDTO.convertToModel();
		account.setRecordStatus(Constants.ACTIVE);
		account = accountRepository.save(account);
		return new AccountResponseDTO(account);
	}

	@Override
	public List<AccountResponseDTO> saveAll(List<AccountRequestDTO> accountRequestDTOList) {
		List<Account> accountList = new ArrayList<>();
		Account account = null;
		for (AccountRequestDTO accountRequestDTO : accountRequestDTOList) {
			account = accountRequestDTO.convertToModel();
			account.setRecordStatus(Constants.ACTIVE);
			accountList.add(account);
		}
		return accountRepository.saveAll(accountList).stream().map(AccountResponseDTO::new).toList();
	}
}
