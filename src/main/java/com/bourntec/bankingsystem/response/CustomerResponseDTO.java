package com.bourntec.bankingsystem.response;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.beans.BeanUtils;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.bourntec.bankingsystem.model.Customer;

import lombok.Getter;
import lombok.Setter;
@Setter
@Getter
public class CustomerResponseDTO {
	private Integer customerId;
	private String name;
	private LocalDate dateOfBirth;
	private Integer phoneNumber;
	private String email;
	@CreatedDate
	private LocalDateTime updatedDate;
	@LastModifiedDate
	private LocalDateTime lastModifiedDate;
	public CustomerResponseDTO(Customer customer) {
		BeanUtils.copyProperties(customer, this);

}
}
