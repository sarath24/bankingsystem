package com.bourntec.bankingsystem.response;

import java.time.LocalDateTime;

import org.springframework.beans.BeanUtils;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.bourntec.bankingsystem.model.Account;
import com.bourntec.bankingsystem.model.AccountType;

import lombok.Getter;
import lombok.Setter;
@Setter
@Getter
public class AccountResponseDTO {
	private Integer accountId;
	private  AccountType type;
    private Integer accountNumber;
    @CreatedDate
	private LocalDateTime updatedDate;
	@LastModifiedDate
	private LocalDateTime lastModifiedDate;
    
    public AccountResponseDTO(Account account) {
    	BeanUtils.copyProperties(account, this);

		
}
}