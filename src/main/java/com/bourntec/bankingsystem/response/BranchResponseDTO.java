package com.bourntec.bankingsystem.response;

import java.time.LocalDateTime;

import org.springframework.beans.BeanUtils;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.bourntec.bankingsystem.model.Branch;

import lombok.Getter;
import lombok.Setter;
@Setter
@Getter
public class BranchResponseDTO {
	private Integer branchId;
    private String bankName;
	private String branchName;
	private String ifscCode;
	@CreatedDate
	private LocalDateTime updatedDate;
	@LastModifiedDate
	private LocalDateTime lastModifiedDate;
	public BranchResponseDTO(Branch branch) {
		BeanUtils.copyProperties(branch, this);
}
}
