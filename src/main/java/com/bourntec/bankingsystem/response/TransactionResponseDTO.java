package com.bourntec.bankingsystem.response;

import java.time.LocalDateTime;

import org.springframework.beans.BeanUtils;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.bourntec.bankingsystem.model.Transaction;
import com.bourntec.bankingsystem.model.TransactionType;

import lombok.Getter;
import lombok.Setter;
@Setter
@Getter
public class TransactionResponseDTO {
	private Integer transactionId;
	private Integer referenceId;
	private Integer amount;
    private TransactionType type;
	private String notification;
	@CreatedDate
	private LocalDateTime updatedDate;
	@LastModifiedDate
	private LocalDateTime lastModifiedDate;
	 
	 public TransactionResponseDTO(Transaction transaction) {
		 BeanUtils.copyProperties(transaction, this);

	}
}
