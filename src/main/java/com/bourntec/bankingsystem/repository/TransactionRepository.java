package com.bourntec.bankingsystem.repository;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.bourntec.bankingsystem.model.Transaction;
import com.bourntec.bankingsystem.response.TransactionResponseDTO;
@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer>,JpaSpecificationExecutor<Transaction> {
	List<Transaction> findByRecordStatus(String recordStatus);
	
    Transaction findByRecordStatusAndtransactionId(String active, int transactionId);
    
    Transaction findByRecordStatusAndType(String active, String type);
    
	}


