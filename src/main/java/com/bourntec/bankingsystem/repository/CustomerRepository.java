package com.bourntec.bankingsystem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import com.bourntec.bankingsystem.model.Customer;
import com.bourntec.bankingsystem.request.CustomerRequestDTO;
import com.bourntec.bankingsystem.response.CustomerResponseDTO;
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer>,JpaSpecificationExecutor<Customer> {
	List<Customer> findByRecordStatus(String recordStatus);
	
    Customer findByRecordStatusAndCustomerId(String active, int customerId);
    
    Customer findByRecordStatusAndName(String active, String name);
    
   
  
	}


