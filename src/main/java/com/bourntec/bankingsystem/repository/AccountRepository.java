package com.bourntec.bankingsystem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.bourntec.bankingsystem.model.Account;
@Repository
public interface AccountRepository extends JpaRepository<Account, Integer>,JpaSpecificationExecutor<Account>{
	List<Account> findByRecordStatus(String recordStatus);
	
    Account findByRecordStatusAndAccountId(String active, int accountId);
    
    Account findByRecordStatusAndAccountNumber(String active, int accountnumber);

	//Optional<Account> findByAccountId(int accountId);


   
	}

