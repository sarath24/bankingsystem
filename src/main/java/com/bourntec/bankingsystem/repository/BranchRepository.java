package com.bourntec.bankingsystem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.bourntec.bankingsystem.model.Branch;
import com.bourntec.bankingsystem.request.BranchRequestDTO;
import com.bourntec.bankingsystem.response.BranchResponseDTO;
@Repository
public interface BranchRepository extends JpaRepository<Branch, Integer>,JpaSpecificationExecutor<Branch> {
	List<Branch> findByRecordStatus(String recordStatus);

     Branch findByRecordStatusAndbranchId(String active, int branchId);
     
     Branch findByRecordStatusAndIfsccode(String active, int ifsccode);
     
   
	}

