package com.bourntec.bankingsystem.request;


import javax.validation.constraints.NotNull;

import org.springframework.beans.BeanUtils;

import com.bourntec.bankingsystem.model.Transaction;
import com.bourntec.bankingsystem.model.TransactionType;

import lombok.Getter;
@Getter
public class TransactionRequestDTO {
	Integer referenceId;
	Integer amount;
	@NotNull(message = "type cannot be empty")
    TransactionType type;
	 String notification;
	 
	 public Transaction  convertToModel() {
		Transaction transaction=new Transaction();
		BeanUtils.copyProperties(this, transaction);
		return transaction;
}
}
