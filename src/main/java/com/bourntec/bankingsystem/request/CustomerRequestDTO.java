package com.bourntec.bankingsystem.request;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;

import org.springframework.beans.BeanUtils;

import com.bourntec.bankingsystem.model.Customer;
import lombok.Setter;

@Setter
public class CustomerRequestDTO {
	   @NotBlank(message = "name cannot be empty")
		String name;
		LocalDate dateOfBirth;
		@NotBlank(message = "phone number cannot be empty")
		Integer phoneNumber;
		String email;
		public Customer convertToModel() {
			Customer customer=new Customer();
			BeanUtils.copyProperties(this, customer);
			return customer;
	}
}
