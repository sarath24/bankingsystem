package com.bourntec.bankingsystem.request;

import javax.validation.constraints.NotBlank;

import org.springframework.beans.BeanUtils;

import com.bourntec.bankingsystem.model.Branch;

import lombok.Getter;
import lombok.Setter;
@Getter

public class BranchRequestDTO {
    String bankName;
	String branchName;
	@NotBlank(message = "ifsc code cannot be empty")
	String ifscCode;
	public Branch convertToModel() {
		Branch branch=new Branch();
		BeanUtils.copyProperties(this, branch);
		return branch;
}
}