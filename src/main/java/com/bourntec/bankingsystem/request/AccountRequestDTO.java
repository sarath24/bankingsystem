package com.bourntec.bankingsystem.request;


import javax.validation.constraints.NotBlank;

import org.springframework.beans.BeanUtils;

import com.bourntec.bankingsystem.model.Account;
import com.bourntec.bankingsystem.model.AccountType;

import lombok.Getter;

@Getter
public class AccountRequestDTO {
	 AccountType type;
	 @NotBlank(message = "account number cannot be empty")
     Integer accountNumber;
     public Account convertToModel() {
    		Account account=new Account();
    		BeanUtils.copyProperties(this, account);
    		
    		return account;
}
}